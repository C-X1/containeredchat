#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPT_PATH
docker build . --file chat.dockerfile --tag chat_container


docker run --rm -it \
    -u root \
    -v $SCRIPT_PATH/data/gpt4all:/data/gpt4all \
    chat_container bash


# git clone --recurse-submodules https://github.com/nomic-ai/gpt4all
