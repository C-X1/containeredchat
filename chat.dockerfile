FROM nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04

ARG USERNAME=ai_user
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV TZ UTC
ENV TERM linux
ENV DEBIAN_FRONTEND noninteractive

RUN echo "UTC" > /etc/timezone

RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -yq --no-install-recommends \
        aria2 \
        bash \
        build-essential \
        bzip2 \
        ca-certificates \
        curl \
        git \
        git-core \
        git-lfs \
        libcairo2-dev \
        libffi-dev \
        libgl1 \
        libglib2.0-0 \
        libgoogle-perftools-dev \
        libgoogle-perftools4  \
        libopencv-dev \
        libpython3-dev \
        libreadline8 \
        libsqlite3-dev \
        libssl-dev \
        libtcmalloc-minimal4 \
        netbase \
        openssh-client \
        pkg-config  \
        procps \
        psmisc \
        python-is-python3 \
        python3 \
        python3-dev \
        python3-opencv \
        python3-pip \
        python3-venv \
        sudo \
        tzdata \
        vim \
        wget \
    && groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && apt-get clean autoclean \
    && rm -rf /var/lib/apt/lists/*

# install Qt6.5 OpenSource
RUN pip install aqtinstall

RUN aqt install-qt -O /opt/Qt linux desktop 6.6.0 gcc_64 -m qthttpserver qtpdf qtwebsockets
RUN aqt install-tool -O /opt/Qt linux desktop tools_cmake
RUN aqt install-tool -O /opt/Qt linux desktop tools_ninja

RUN apt-get update \
    && apt-get install -yq --no-install-recommends \
            libglx-dev \
            libxmu-dev \
            libxi-dev \
            libgl1-mesa-dev \
    && apt-get clean autoclean \
    && rm -rf /var/lib/apt/lists/*

ENV QT_PATH=/opt/Qt
ENV QT_GCC=/opt/Qt/6.6.0/gcc_64
ENV PATH=/opt/Qt/Tools/CMake/bin:/opt/Qt/Tools/Ninja:/opt/Qt/6.6.0/gcc_64/bin:$PATH
ENV Qt6_DIR=/opt/Qt/6.6.0/gcc_64/lib/cmake/Qt6
##aqt list-qt linux desktop --long-modules latest gcc_64

### NOVNC
RUN apt-get update \
    && apt-get install -yq --no-install-recommends \
      bash \
      fluxbox \
      git \
      net-tools \
      novnc \
      supervisor \
      x11vnc \
      xterm \
      xvfb \
    && apt-get clean autoclean \
    && rm -rf /var/lib/apt/lists/*

ENV HOME=/home/$USERNAME \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_ALL=C.UTF-8 \
    DISPLAY=:0.0 \
    DISPLAY_WIDTH=1440 \
    DISPLAY_HEIGHT=900 \
    RUN_XTERM=yes \
    RUN_FLUXBOX=yes


RUN git clone https://github.com/theasp/docker-novnc.git  /app

RUN mkdir -p /home/$USERNAME \
    && chown  $USERNAME:$USERNAME /home/$USERNAME

WORKDIR /home/$USERNAME

USER $USERNAME

### RUN NOVNC
COPY . /app
CMD ["/app/entrypoint.sh"]
EXPOSE 8080
