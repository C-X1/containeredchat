#!/bin/bash

DATA_DIR=/data/private_gpt
INSTALL_FINISHED_FILE=$DATA_DIR/installation_finished


## Create venv and activate it
python3 -m venv $DATA_DIR/venv
source $DATA_DIR/venv/bin/activate

if [ ! -f $INSTALL_FINISHED_FILE ]; then
    echo "Installing files for private-gpt"

    mkdir -p $DATA_DIR/../doc
    mkdir -p $DATA_DIR/{src,db,cache}

    git clone https://github.com/imartinez/privateGPT.git $DATA_DIR/src

    pip3 install -r $DATA_DIR/src/requirements.txt

    touch $INSTALL_FINISHED_FILE
fi

export PERSIST_DIRECTORY=$DATA_DIR/db
export MODEL_TYPE=GPT4All
export MODEL_PATH=/data/models/gpt_4all/ggml-gpt4all-j-v1.3-groovy.bin
export EMBEDDINGS_MODEL_NAME=all-MiniLM-L6-v2
export MODEL_N_CTX=1000
export MODEL_N_BATCH=8
export TARGET_SOURCE_CHUNKS=4

if [ ! -v $RUN ]; then
    echo RUNNING
    # CPU LIMIT
    [[ ! $CPU_THREADS ]] && CPU_THREADS=$(nproc)

    cd $DATA_DIR/src
    bash
fi
