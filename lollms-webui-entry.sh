#!/bin/bash

INSTALL_FINISHED_FILE=/data/lollms-webui/installation_finished
PATH_CONFIG_FILE=/data/lollms-webui/configs/global_paths_cfg.yaml
FIRST_MODEL_PATH=/data/models/llama_cpp_official/Wizard-Vicuna-13B-Uncensored.ggmlv3.q4_K_S.bin

## Create venv and activate it
python3 -m venv /data/lollms-webui/venv
source /data/lollms-webui/venv/bin/activate

if [ ! -f $INSTALL_FINISHED_FILE ]; then
    echo "Installing files for lollms-webui"

    mkdir -p /data/lollms-webui/{configs,data,venv,src}
    mkdir -p /data/models/{py_llama_cpp,c_transformers,llama_cpp_official,binding_template,gpt_j_m,gpt_4all,open_ai,gpt_j_a,gptq,hugging_face}

    # Config Paths
    printf 'lollms_path: /data/lollms-webui/src/lollms\n' > $PATH_CONFIG_FILE
    printf 'lollms_personal_path: /home/ai_user/Documents/lollms\n' >> $PATH_CONFIG_FILE


    pip install --upgrade pip setuptools wheel virtualenv
    pip install torch torchvision torchaudio --pre -f https://download.pytorch.org/whl/nightly/cu121/torch_nightly.html
    pip install git+https://github.com/abetlen/llama-cpp-python.git

    git clone --recurse-submodules https://github.com/ParisNeo/lollms.git /data/lollms-webui/src/lollms
    cd /data/lollms-webui/src/lollms
    pip install -r requirements.txt -e .

    git clone --recurse-submodules https://github.com/ParisNeo/lollms-webui.git /data/lollms-webui/src/lollms-webui
    cd /data/lollms-webui/src/lollms-webui
    pip install -r requirements.txt -e .

    cp /data/lollms-webui/src/lollms-webui/configs/* /data/lollms-webui/configs

    #cd /data/models/llama_cpp_official
    #aria2c https://huggingface.co/TheBloke/Wizard-Vicuna-13B-Uncensored-GGML/resolve/main/Wizard-Vicuna-13B-Uncensored.ggmlv3.q4_K_S.bin

    touch $INSTALL_FINISHED_FILE
fi

if [ ! -v $RUN ]; then
    echo RUNNING
    # CPU LIMIT
    [[ ! $CPU_THREADS ]] && CPU_THREADS=$(nproc)

    cd /data/lollms-webui/src/lollms-webui
    python app.py -m Wizard-Vicuna-13B-Uncensored.ggmlv3.q4_K_S.bin --host 0.0.0.0 --port 8080 --n_threads "$CPU_THREADS"
fi
