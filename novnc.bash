#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPT_PATH
docker build . --file chat.dockerfile --tag chat_container

mkdir -p $SCRIPT_PATH/data

docker run --rm -it \
    -e RUN=TRUE \
    -v $SCRIPT_PATH/data:/data \
    -v $SCRIPT_PATH/data/lollms-webui/data:/home/ai_user/Documents/lollms \
    -v $SCRIPT_PATH/data/models:/home/ai_user/Documents/lollms/models \
    -v $SCRIPT_PATH/data/lollms-webui/configs:/data/lollms-webui/src/lollms-webui/configs \
    -v $SCRIPT_PATH/data/lollms-webui/configs/global_paths_cfg.yaml:/data/lollms-webui/src/lollms-webui/global_paths_cfg.yaml \
    -p 8080:8080 \
    chat_container
