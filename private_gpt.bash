#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPT_PATH
docker build . --file chat.dockerfile --tag chat_container

mkdir -p $SCRIPT_PATH/data

docker run --rm -it \
    -v $SCRIPT_PATH/data:/data \
    -v $SCRIPT_PATH/private_gpt-entry.sh:/entry.sh:ro \
    --entrypoint /entry.sh chat_container

echo "RUN"

docker run --rm -it \
    -e RUN=TRUE \
    -v $SCRIPT_PATH/data:/data \
    -v $SCRIPT_PATH/data/doc:/data/private_gpt/src/source_documents \
    -v $SCRIPT_PATH/data/private_gpt/cache:/home/ai_user/.cache:rw \
    -v $SCRIPT_PATH/private_gpt-entry.sh:/entry.sh:ro \
    --entrypoint /entry.sh \
    chat_container
